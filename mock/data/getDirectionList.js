module.exports = {
    message: null,
    data: {
        VISUAL_TYPE: '可视化方向',
        FRAME_TYPE: '框架方向',
        MANAGE_SYSTEM_TYPE: '中台方向',
        BUSINESS_TYPE: '具体业务'
    },
    success: true,
    all: 0
}
