module.exports = {
    message: null,
    data: [
        {
            pid: '001',
            pname: '费昱佳',
            premark: '踏实努力',
            pskill: 'VISUAL_TYPE'
        },
        {
            pid: '002',
            pname: '陈瑞',
            premark: '乐观开朗',
            pskill: 'VISUAL_TYPE'
        },
        {
            pid: '003',
            pname: '陈宁',
            premark: '乐观',
            pskill: 'MANAGE_SYSTEM_TYPE'
        },
        {
            pid: '004',
            pname: '葛王杰',
            premark: '实力派',
            pskill: 'BUSINESS_TYPE'
        },
        {
            pid: '005',
            pname: '季延炜',
            premark: '温和',
            pskill: 'FRAME_TYPE'
        }
    ],
    success: true,
    all: 5
}
