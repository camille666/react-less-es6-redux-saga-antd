module.exports = {
    '/api/list*': '/getList',
    '/api/deleteItem*': '/deleteItem',
    '/api/updateItem*': '/updateItem',
    '/api/addItem*': '/addItem',
    '/api/dirList*': '/getDirectionList'
}
