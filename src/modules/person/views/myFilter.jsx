import React from 'react'

import { Form, Button, Input, Select, Row, Col } from 'antd'
const { Option } = Select

class MyFilter extends React.Component {
    getPersonList = () => {
        this.props.getPersonList && this.props.getPersonList()
    }

    changeFilter(data) {
        this.props.changeFilter && this.props.changeFilter(data)
    }

    handlePageState = () => {
        this.props.changePageState && this.props.changePageState()
    }

    handleEnterButton = event => {
        if (event.keyCode === 13) {
            this.getPersonList()
        }
    }

    render() {
        const { filterParam, dirList } = this.props
        const { getFieldDecorator } = this.props.form
        return (
            <div style={{ marginBottom: '20px' }}>
                <Row style={{ marginBottom: '20px' }}>
                    <Col span={8}>
                        <span style={{ marginRight: '5px' }}>编号</span>
                        {getFieldDecorator('pid', {
                            initialValue: filterParam.pid
                        })(
                            <Input
                                style={{ width: '180px' }}
                                placeholder="请输入编号"
                                onChange={e =>
                                    this.changeFilter({
                                        pid: e.target.value
                                    })
                                }
                                onKeyDown={this.handleEnterButton}
                            />
                        )}
                    </Col>
                    <Col span={8}>
                        <span style={{ marginRight: '5px' }}>姓名</span>
                        {getFieldDecorator('pname', {
                            initialValue: filterParam.pname
                        })(
                            <Input
                                style={{ width: '180px' }}
                                placeholder="请输入姓名"
                                onChange={e =>
                                    this.changeFilter({
                                        pname: e.target.value
                                    })
                                }
                                onKeyDown={this.handleEnterButton}
                            />
                        )}
                    </Col>
                    <Col span={8}>
                        <span style={{ marginRight: '5px' }}>前端方向</span>
                        {getFieldDecorator('pskill', {
                            initialValue: filterParam.pskill
                        })(
                            <Select
                                style={{ width: '150px' }}
                                onChange={value => {
                                    this.changeFilter({ pskill: value })
                                    this.getPersonList()
                                }}
                            >
                                <Option key="-1">全部</Option>
                                {dirList.map(item => {
                                    return (
                                        <Option value={item.id} key={item.id}>
                                            {item.name}
                                        </Option>
                                    )
                                })}
                            </Select>
                        )}
                    </Col>
                </Row>
                <Row>
                    <Col span={8}>
                        <Button
                            type="primary"
                            size="large"
                            onClick={() => {
                                this.getPersonList()
                            }}
                        >
                            查询
                        </Button>
                    </Col>
                    <Col span={8}>
                        <Button
                            size="large"
                            onClick={() => {
                                this.changeFilter({
                                    pskill: '',
                                    pid: '',
                                    pname: ''
                                })
                                this.getPersonList()
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                    <Col span={8}>
                        <Button
                            type="primary"
                            size="large"
                            onClick={this.handlePageState}
                        >
                            新增
                        </Button>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Form.create({
    mapPropsToFields(props) {
        const { filterParam } = props
        return {
            pskill: Form.createFormField({
                value: filterParam.pskill
            }),
            pid: Form.createFormField({
                value: filterParam.pid
            }),
            pname: Form.createFormField({
                value: filterParam.pname
            })
        }
    }
})(MyFilter)
