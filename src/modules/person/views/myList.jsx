import React from 'react'
import { Table, Popconfirm, Spin } from 'antd'

class MyList extends React.Component {
    constructor(props) {
        super(props)
        this.columns = [
            {
                title: '编号',
                dataIndex: 'pid',
                key: 'pid',
                width: '20%'
            },
            {
                title: '姓名',
                dataIndex: 'pname',
                key: 'pname',
                width: '20%'
            },
            {
                title: '技能',
                dataIndex: 'pskill',
                key: 'pskill',
                width: '20%',
                render: text => {
                    return (
                        <p>
                            <span>{this.props.dirObj[text]}</span>
                        </p>
                    )
                }
            },
            {
                title: '备注',
                dataIndex: 'premark',
                key: 'premark',
                width: '20%'
            },
            {
                title: '操作',
                render: (text, record) => {
                    return (
                        <span>
                            <a
                                onClick={this.handleChangePageState.bind(
                                    this,
                                    record
                                )}
                            >
                                修改
                            </a>
                            <span style={{ padding: '0 10px' }}>|</span>
                            <Popconfirm
                                title="确认删除吗？"
                                onConfirm={this.deleteItem.bind(
                                    this,
                                    record.id
                                )}
                            >
                                <a>删除</a>
                            </Popconfirm>
                        </span>
                    )
                }
            }
        ]
    }

    handleChangePageState = (record = {}) => {
        this.props.changePageState(record)
    }

    deleteItem = id => {
        this.props.deleteItem(id)
    }

    render() {
        const { dataSourceList, loading, total, getNextPageList } = this.props

        const pagination = {
            pageSize: 10,
            showQuickJumper: true,
            total: total,
            showTotal: (total, range) => {
                return (
                    <p className="l">
                        符合条件标签共 <b>{total}</b> 条
                    </p>
                )
            },
            onChange: (page, pageSize) => {
                getNextPageList(page, pageSize)
            }
        }
        return (
            <Spin tip="正在加载数据..." spinning={loading}>
                <Table
                    dataSource={dataSourceList}
                    columns={this.columns}
                    bordered
                    pagination={pagination}
                    rowKey={record => record.pid}
                />
            </Spin>
        )
    }
}
export default MyList
