import React from 'react'

import { Select, Input, Form, Button } from 'antd'

const FormItem = Form.Item
const Option = Select.Option

class MyForm extends React.Component {
    constructor(props) {
        super(props)
        this.formItemLayout = {
            labelCol: {
                span: 6
            },
            wrapperCol: {
                span: 18
            }
        }
    }

    handleSubmit = e => {
        const isEdit = this.props.isEdit
        this.props.form &&
            this.props.form.validateFields((err, values) => {
                if (!err) {
                    if (isEdit) {
                        values.id = this.props.record.id
                    }
                    this.props.handleSubmit(values)
                }
            })
    }

    handleCancel = e => {
        this.props.onCancel && this.props.onCancel()
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const { dirList = [] } = this.props
        const formItemLayout = this.formItemLayout

        return (
            <Form>
                <FormItem {...formItemLayout} label="编号" hasFeedback>
                    {getFieldDecorator('pid', {
                        rules: [
                            {
                                required: true,
                                message: '请填写编号'
                            }
                        ]
                    })(<Input placeholder="编号" />)}
                </FormItem>
                <FormItem {...formItemLayout} label="姓名" hasFeedback>
                    {getFieldDecorator('pname', {
                        rules: [
                            {
                                required: true,
                                message: '请填写姓名'
                            }
                        ]
                    })(<Input placeholder="姓名" />)}
                </FormItem>
                <FormItem {...formItemLayout} label="前端方向" hasFeedback>
                    {getFieldDecorator('pskill', {
                        rules: [
                            {
                                required: true,
                                message: '请填写前端方向'
                            }
                        ]
                    })(
                        <Select placeholder="前端方向">
                            {dirList.map(item => {
                                return (
                                    <Option value={item.id} key={item.id}>
                                        {item.name}
                                    </Option>
                                )
                            })}
                        </Select>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="备注">
                    {getFieldDecorator('premark', {
                        rules: [
                            {
                                required: false,
                                message: '请填写备注'
                            }
                        ]
                    })(<Input placeholder="备注" />)}
                </FormItem>
                <FormItem wrapperCol={{ span: 12, offset: 6 }}>
                    <Button type="primary" onClick={this.handleSubmit}>
                        提交
                    </Button>
                    <Button
                        style={{ marginLeft: '10px' }}
                        onClick={this.handleCancel}
                    >
                        取消
                    </Button>
                </FormItem>
            </Form>
        )
    }
}

export default Form.create({
    mapPropsToFields(props) {
        const mapping = {
            pid: Form.createFormField({
                value: props.record.pid
            }),
            pname: Form.createFormField({
                value: props.record.pname
            }),
            pskill: Form.createFormField({
                value: props.record.pskill
            }),
            premark: Form.createFormField({
                value: props.record.premark
            })
        }
        return mapping
    }
})(MyForm)
