import React from 'react'
import { Modal, notification } from 'antd'
import { connect } from 'react-redux'
import * as Act from '../models/action'

import MyFilter from './myFilter'
import MyList from './myList'
import MyForm from './myForm'

class Person extends React.Component {
    // 组件初始化后执行的方法
    componentDidMount() {
        const { dispatch } = this.props

        dispatch(Act.getListAct())
        dispatch(Act.getDirListAct())
    }

    hideDialog = () => {
        this.props.dispatch(
            Act.setSomeStateAct({
                visible: false
            })
        )
    }

    render() {
        const {
            listData,
            filterParam,
            someState,
            dirList,
            dirObj
        } = this.props.personData
        const { list, total } = listData

        const { loading, visible, record, isEdit } = someState
        const { dispatch } = this.props

        // 搜索栏的props
        const filterProps = {
            filterParam,
            dirList,
            changeFilter(data) {
                dispatch(Act.setFilterParamAct(data))
            },
            getPersonList() {
                dispatch(
                    Act.setFilterParamAct({
                        page: 1,
                        pageSize: 10
                    })
                )
                dispatch(Act.getListAct())
            },
            changePageState() {
                dispatch(
                    Act.setSomeStateAct({
                        visible: true,
                        isEdit: false,
                        record: {}
                    })
                )
            }
        }

        // 列表参数
        const listProps = {
            dataSourceList: list,
            total,
            loading: loading,
            dirObj,
            changePageState(record) {
                dispatch(
                    Act.setSomeStateAct({
                        visible: true,
                        isEdit: true,
                        record
                    })
                )
            },
            deleteItem(id) {
                dispatch(
                    Act.deleteItemAct({ id }, data => {
                        if (data.success) {
                            dispatch(Act.getListAct())
                            notification.success({
                                message: '通知',
                                description: '删除成功'
                            })
                        } else {
                            notification.error({
                                message: '通知',
                                description: '删除失败'
                            })
                        }
                    })
                )
            },
            getNextPageList(page, pageSize) {
                dispatch(
                    Act.setFilterParamAct({
                        page,
                        pageSize
                    })
                )
                dispatch(Act.getListAct())
            }
        }

        // 表单组件参数
        const formProps = {
            dirList,
            isEdit,
            record,
            onCancel: () => {
                this.hideDialog()
            },
            handleSubmit: values => {
                if (isEdit) {
                    dispatch(
                        Act.updateItemAct(values, data => {
                            if (data.success) {
                                notification.success({
                                    message: '通知',
                                    description: '修改成功'
                                })
                                dispatch(Act.getListAct())
                                this.hideDialog()
                            } else {
                                notification.error({
                                    message: '通知',
                                    description: data.msg || '修改提交失败'
                                })
                            }
                        })
                    )
                } else {
                    dispatch(
                        Act.addItemAct(values, data => {
                            if (data.success) {
                                notification.success({
                                    message: '通知',
                                    description: '新增成功'
                                })
                                dispatch(Act.getListAct())
                                this.hideDialog()
                            } else {
                                notification.error({
                                    message: '通知',
                                    description: data.msg || '新增提交失败'
                                })
                            }
                        })
                    )
                }
            }
        }

        return (
            <div>
                <MyFilter {...filterProps} />
                <MyList {...listProps} />
                {visible && (
                    <Modal
                        visible={visible}
                        title={isEdit ? '修改候选人' : '新增候选人'}
                        footer={null}
                        onCancel={this.hideDialog}
                    >
                        <MyForm {...formProps} />
                    </Modal>
                )}
            </div>
        )
    }

    destroyState() {
        this.props.dispatch(Act.destroySomeStateAct())
    }
}
Person.defaultProps = {}
const mapStateToProps = state => {
    return {
        personData: state.personReducer
    }
}
export default connect(mapStateToProps)(Person)
