export const GET_LIST = 'getList'
export const SET_LIST = 'setList'
export const SET_SOME_STATE = 'setSomeState'
export const SET_FILTER_PARAM = 'setFilterParam'
export const ADD_ITEM = 'addItem'
export const UPDATE_ITEM = 'updateItem'
export const DELETE_ITEM = 'deleteItem'
export const GET_DIR_LIST = 'getDirList'
export const SET_DIR_LIST = 'setDirList'
export const DESTROY_SOME_STATE = 'destroySomeState'

export function getListAct() {
    return {
        type: GET_LIST
    }
}

/**
 * 设置列表
 * @param {*返回数据} data
 */
export function setListAct(data) {
    return {
        type: SET_LIST,
        data
    }
}

export function setSomeStateAct(data) {
    return {
        type: SET_SOME_STATE,
        data
    }
}

/**
 * 修改筛选参数
 * @param {*参数} data
 */
export function setFilterParamAct(data) {
    return {
        type: SET_FILTER_PARAM,
        data
    }
}

export function addItemAct(param, callback) {
    return {
        type: ADD_ITEM,
        param,
        callback
    }
}

/**
 * 列表删除
 * @param {*删除数据的id} id
 */
export function deleteItemAct(param, callback) {
    return {
        type: DELETE_ITEM,
        param,
        callback
    }
}

/**
 * 提交表单
 * @param {*} param
 * @param {*} callback
 */
export function updateItemAct(param, callback) {
    return {
        type: UPDATE_ITEM,
        param,
        callback
    }
}

export function getDirListAct(param) {
    return {
        type: GET_DIR_LIST,
        param
    }
}

export function setDirListAct(data) {
    return {
        type: SET_DIR_LIST,
        data
    }
}

export function destroySomeStateAct(data) {
    return {
        type: DESTROY_SOME_STATE,
        data
    }
}
