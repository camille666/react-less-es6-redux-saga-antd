import { select, put, call, takeEvery, fork, all } from 'redux-saga/effects'
import * as Act from './action'
import * as Fetch from '../api'

function* getDirList(action) {
    const res = yield call(Fetch.getDirList)
    yield put(Act.setDirListAct(res.data))
}

function* _getDirList() {
    yield takeEvery(Act.GET_DIR_LIST, getDirList)
}

function* getList() {
    yield put(
        Act.setSomeStateAct({
            loading: true
        })
    )
    const filterParam = yield select(state => {
        return state.personReducer.filterParam
    })

    const res = yield call(Fetch.getList, filterParam)

    yield put(Act.setListAct(res))
    yield put(
        Act.setSomeStateAct({
            loading: false
        })
    )
}

function* _getList() {
    yield takeEvery(Act.GET_LIST, getList)
}

function* deleteItem(action) {
    const res = yield call(Fetch.deleteItem, action.param)
    action.callback && action.callback(res)
}

function* _deleteItem() {
    yield takeEvery(Act.DELETE_ITEM, deleteItem)
}

function* addItem(action) {
    yield put(
        Act.setSomeStateAct({
            submitLoading: true
        })
    )
    const res = yield call(Fetch.addItem, action.param)
    action.callback && action.callback(res)
    yield put(
        Act.setSomeStateAct({
            submitLoading: false
        })
    )
}

function* _addItem() {
    yield takeEvery(Act.ADD_ITEM, addItem)
}

function* updateItem(action) {
    yield put(
        Act.setSomeStateAct({
            submitLoading: true
        })
    )
    const res = yield call(Fetch.updateItem, action.param)
    action.callback && action.callback(res)
    yield put(
        Act.setSomeStateAct({
            submitLoading: false
        })
    )
}

function* _updateItem() {
    yield takeEvery(Act.UPDATE_ITEM, updateItem)
}

export default function* root() {
    yield all([
        fork(_getDirList),
        fork(_getList),
        fork(_addItem),
        fork(_deleteItem),
        fork(_updateItem)
    ])
}
