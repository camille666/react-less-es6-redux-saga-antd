import * as Act from './action'
import { cloneDeep, extend } from 'lodash'

const initState = {
    listData: {
        list: [],
        total: 0
    },
    someState: {
        loading: false,
        submitLoading: false,
        visible: false,
        record: {},
        isEdit: false
    },
    filterParam: {
        pskill: '',
        pid: '',
        pname: '',
        pageSize: 10,
        page: 1
    },
    dirList: [], // 前端根据接口返回对象拼接的数组，用于下拉列表遍历。
    dirObj: {} // 接口返回数据，用于人员列表匹配的skill渲染文本。
}

export default function oneReducer(state = initState, action) {
    let newState = cloneDeep(state)
    const result = action.data
    const dirArray = []

    switch (action.type) {
        case Act.SET_LIST:
            newState = extend(newState, {
                listData: {
                    list: result.data.data || [],
                    total: result.all || 0
                }
            })

            return newState

        case Act.SET_SOME_STATE:
            newState = extend(newState, {
                someState: Object.assign(newState.someState, result)
            })
            return newState

        case Act.SET_FILTER_PARAM:
            newState = extend(newState, {
                filterParam: Object.assign(newState.filterParam, result)
            })
            return newState

        case Act.SET_DIR_LIST:
            for (const key in result.data) {
                dirArray.push({
                    id: key,
                    name: result.data[key]
                })
            }
            newState = Object.assign({}, newState, {
                dirList: dirArray,
                dirObj: result.data
            })
            return newState

        case Act.DESTROY_SOME_STATE:
            return newState
        default:
            return state
    }
}
