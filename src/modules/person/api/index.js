import axios from 'axios'
/**
 * 获取列表
 */
export async function getList(params) {
    return axios.get('/api/list', { data: params })
}

/**
 * 获取下拉列表
 */
export async function getDirList(params) {
    return axios.get('/api/dirList', { data: params })
}

/**
 * 删除
 */
export async function deleteItem(params) {
    return axios.get('/api/deleteItem', { data: params })
}

/**
 * 更新
 */
export async function updateItem(params) {
    return axios.post('/api/updateItem', { data: params })
}

/**
 * 添加
 */
export async function addItem(params) {
    return axios.post('/api/addItem', { data: params })
}
