import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Router, Redirect, Route, Switch } from 'react-router-dom'

import { ConfigProvider } from 'antd'
// 由于 antd 组件的默认文案是英文，所以需要修改为中文
import zhCN from 'antd/es/locale/zh_CN'
// import moment from 'moment';
// import 'moment/locale/zh-cn';

import HomePage from 'modules/index'
import 'styles/base.css'
import store from './store/store'
const createHistory = require('history').createBrowserHistory

class App extends React.Component {
    render() {
        return (
            <ConfigProvider locale={zhCN}>
                <Provider store={store}>
                    <Router history={createHistory()}>
                        <Switch>
                            <Route
                                path="/"
                                exact={true}
                                render={() => <Redirect to="/list" />}
                            />
                            <Route path="/list" component={HomePage} />
                        </Switch>
                    </Router>
                </Provider>
            </ConfigProvider>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'))
