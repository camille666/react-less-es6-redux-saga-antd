import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import createSagaMiddleware from 'redux-saga'
import loggerMiddleware from './loggerMiddleware'
// 引入业务模块的reducers和sagas
import reducers from './reducers'
import rootSaga from './sagas'

const createHistory = require('history').createBrowserHistory
const sagaMiddleware = createSagaMiddleware()

// 打印日志
const middlewares = []
middlewares.push(routerMiddleware(createHistory()))
if (process.env.NODE_ENV === 'development') {
    // 加入中间件logger
    middlewares.push(loggerMiddleware)
}
middlewares.push(sagaMiddleware)

// store
let store
if (
    !(
        window.__REDUX_DEVTOOLS_EXTENSION__ ||
        window.__REDUX_DEVTOOLS_EXTENSION__
    )
) {
    store = createStore(reducers, applyMiddleware(...middlewares))
} else {
    store = createStore(
        reducers,
        compose(
            applyMiddleware(...middlewares),
            window.__REDUX_DEVTOOLS_EXTENSION__ &&
                window.__REDUX_DEVTOOLS_EXTENSION__()
        )
    )
}
sagaMiddleware.run(rootSaga)

export default store
