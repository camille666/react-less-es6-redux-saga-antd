import { combineReducers } from 'redux'
import personReducer from 'modules/person/models/reducer'

const reducers = combineReducers({
    personReducer
})

export default reducers
