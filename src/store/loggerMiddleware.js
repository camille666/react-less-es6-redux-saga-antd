import { createLogger } from 'redux-logger'

const actionTransformer = action => {
    return action
}

const level = global.loggerLevel || 'log'

const logger = {}

for (const method in console) {
    if (typeof console[method] === 'function') {
        logger[method] = console[method].bind(console)
    }
}

logger[level] = function levelFn(...args) {
    const lastArg = args.pop()

    if (Array.isArray(lastArg)) {
        return lastArg.forEach(item => {
            // console[level].apply(console, [...args, item])
        })
    }
    console[level](...arguments)
}

export default createLogger({
    level,
    actionTransformer,
    logger
})
