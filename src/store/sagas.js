import { fork, all } from 'redux-saga/effects'
import personSaga from 'modules/person/models/saga'

const sagas = [personSaga]

function* rootSaga() {
    yield all(
        sagas.map(fn => {
            return fn && fork(fn)
        })
    )
}
export default rootSaga
